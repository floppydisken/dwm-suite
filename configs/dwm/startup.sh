#!/bin/env bash

silent() {
    $@ &> /dev/null &
}

feh --bg-scale "$HOME/Pictures/cyberpunkcity.jpg"
setxkbmap -layout us,dk -option "grp:ctrls_toggle"

silent redshift
silent dwmblocks
