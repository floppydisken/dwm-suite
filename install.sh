install_deps() {
    echo "Installing dependencies"
    sudo apt install libxft-dev \
        kitty \
        rofi \
        fonts-firacode \
        redshift \
        playerctl
}

fullpath() {
    readlink -f $1
}

setup_configs() {
    echo "Setting up configs"
    ln -s $(fullpath configs/redshift.conf) ~/.config/redshift.conf
    ln -s $(fullpath configs/dwm) ~/.config/dwm
}

cleanup() {
    echo "Removing symlinks if they exist"
    unlink ~/.config/redshift.conf
    unlink ~/.config/dwm
}

install_dwm() {
    echo "Installing dwm"
    (cd "dwm-6.2/"; sudo make clean install)
}

install_dwmblocks() {
    echo "Installing dwmblocks"
    (cd dwmblocks; sudo make clean install installscripts)
}

install_deps
echo
install_dwm
echo
install_dwmblocks
cleanup
echo
setup_configs
