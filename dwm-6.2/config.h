#include <X11/XF86keysym.h>

/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Fira Code:size=10", "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";

/* https://www.nordtheme.com/docs/colors-and-palettes */
/* Nord: Polar night */
static const char color_nord0[] = "#2e3440";
static const char color_nord1[] = "#3b4252";
static const char color_nord2[] = "#434C5E";
static const char color_nord3[] = "#4c556a";

/* Nord: Snow Storm */
static const char color_nord4[] = "#d8dee9";
static const char color_nord5[] = "#e5e9f0";
static const char color_nord6[] = "#eceff4";

/* Nord: Frost */
static const char color_nord10[] = "#5E81AC";

/* Whatever */
static const char col_norm_fg[] = "#bbbbbb";
static const char color_charcoal[] = "#264653";
static const char color_sandy_brown[] = "#F4A261";
static const char color_burnt_sienna[] = "#E76F51";
static const char col_norm_border[] = "#F4A261";

static const char col_sel_fg[] = "#eeeeee";
static const char col_sel_bg[] = "#2A9D8F";
static const char col_sel_border[] = "#2A9D8F";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	// [SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	// [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
	[SchemeNorm] = { color_nord4, color_nord0, color_nord0 },
	[SchemeSel]  = { color_nord6, color_nord10,  color_nord10 },
	// [SchemeNorm] = { col_norm_fg, col_norm_bg, col_norm_border },
	// [SchemeSel]  = { col_sel_fg,  col_sel_bg,  col_sel_border  },
};

/* tagging */
static const char *tags[] = { "1 - >_", "2 - Frontend", "3 - Backend", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.7; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *rofi[] = { "rofi", "-show", "run" };
static const char *termcmd[]  = { "kitty" };

/* Add somewhere in your constants definition section */
// amixer -q -D pulse sset Master 10%+
static const char *upvol[]   = { "/usr/bin/amixer", "-q", "-D", "pulse", "sset", "Master", "5%+", NULL };
static const char *downvol[]   = { "/usr/bin/amixer", "-q", "-D", "pulse", "sset", "Master", "5%-", NULL };
static const char *togglevol[]   = { "/usr/bin/amixer", "-q", "-D", "pulse", "sset", "Master", "1+", "toggle", NULL };

static const char playerctlbin[] = "/usr/bin/playerctl";
static const char *playnext[] = { playerctlbin, "next" };
static const char *playpause[] = { playerctlbin, "play-pause" };
static const char *playprev[] = { playerctlbin, "prev" };


static const char *signal_dwmblocks_alsa[] = { "/usr/bin/pkill", "-RTMIN+3", "dwmblocks", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
/* Add to keys[] array. With 0 as modifier, you are able to use the keys directly. */
	// SIGNAL(0, XF86XK_AudioRaiseVolume, spawn, {.v = upvol}, 3, "dwmblocks")
	// SIGNAL(0, XF86XK_AudioLowerVolume, spawn, {.v = downvol}, 3, "dwmblocks")
	{ 0,                       		XF86XK_AudioRaiseVolume, spawn, {.v = upvol   } },
	{ 0,                       		XF86XK_AudioRaiseVolume, spawn, {.v = signal_dwmblocks_alsa } },
	{ 0,                       		XF86XK_AudioLowerVolume, spawn, {.v = downvol } },
	{ 0,                       		XF86XK_AudioLowerVolume, spawn, {.v = signal_dwmblocks_alsa } },
	{ 0,                       		XF86XK_AudioMute, spawn, {.v = togglevol } },
    { 0,                            XF86XK_AudioPlay, spawn, {.v = playpause } },
    { 0,                            XF86XK_AudioNext, spawn, {.v = playnext } },
    { 0,                            XF86XK_AudioPrev, spawn, {.v = playprev } },
	{ MODKEY,                       XK_p,      spawn,          {.v = rofi } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

